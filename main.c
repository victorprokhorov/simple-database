#include <ctype.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

typedef struct {
  bool verbose;
  size_t buffer_statement_size;
  size_t table_title_size;
  size_t varchar_size;
} Settings;

typedef struct {
  char *statement;
  size_t size;
} Buffer;

void PrintPrompt() { printf("> "); }

void ReadPrompt(Buffer *prompt_buffer, Settings *settings) {
  ssize_t r =
      getline(&(prompt_buffer->statement), &(prompt_buffer->size), stdin);
  prompt_buffer->statement[--r] = '\0';
  settings->buffer_statement_size = prompt_buffer->size;
  if (settings->verbose) {
    printf("Buffer size %lu\n", prompt_buffer->size);
    printf("Statement size %lu\n", strlen(prompt_buffer->statement));
  }
}

Buffer *CreateBuffer() {
  Buffer *prompt_buffer = (Buffer *)malloc(sizeof(Buffer));
  prompt_buffer->statement = NULL;
  prompt_buffer->size = 0;
  return prompt_buffer;
}

void FreeBuffer(Buffer *prompt_buffer) {
  free(prompt_buffer->statement);
  free(prompt_buffer);
}

Settings *CreateSettings() {
  Settings *settings = malloc(sizeof(Settings));
  settings->verbose = false;
  settings->table_title_size = 255;
  settings->varchar_size = 255;
  return settings;
}

void FreeSettings(Settings *settings) { free(settings); }

const char *kStatementMetaKeywords[3] = {".help", ".quit", ".verbose"};
const char *kStatementKeywords[9] = {"CREATE", "TABLE",   "INTO",
                                     "INT",    "VARCHAR", "INSERT",
                                     "VALUES", "SELECT",  "FROM"};

int NormalizeStatement(char *statement, Settings *settings) {
  if (strncmp(statement, ".", strlen(".")) == 0) {
    char *p;
    int meta_keywords_found = 0;
    char kUpperStatementMetaKeywords[3][settings->buffer_statement_size];

    for (int i = 0; i < (int)(sizeof kStatementMetaKeywords /
                              sizeof *kStatementMetaKeywords);
         ++i) {
      int j = 0;
      while (kStatementMetaKeywords[i][j] != '\0') {
        kUpperStatementMetaKeywords[i][j] =
            (char)toupper(kStatementMetaKeywords[i][j]);
        ++j;
      }
      kUpperStatementMetaKeywords[i][j] = '\0';
    }

    for (int j = 0; j < (int)(sizeof kStatementMetaKeywords /
                              sizeof *kStatementMetaKeywords);
         ++j) {
      if ((p = strstr(statement, kStatementMetaKeywords[j])) != NULL) {
        ++meta_keywords_found;
      } else if ((p = strstr(statement, kUpperStatementMetaKeywords[j])) !=
                 NULL) {
        while ((p = strstr(statement, kUpperStatementMetaKeywords[j])) !=
               NULL) {
          int k = 0;
          while (kUpperStatementMetaKeywords[j][k] != '\0') {
            *(p + k) = (char)tolower(*(p + k));
            ++k;
          }
          ++meta_keywords_found;
        }
      }
    }
    if (meta_keywords_found == 1) return EXIT_SUCCESS;
    printf("Syntax error.\n");
    return EXIT_FAILURE;
  }

  int i = 0;
  int balance = 0;
  int semicolons = 0;
  while (statement[i]) {
    if (statement[i] == '(') {
      ++balance;
    } else if (statement[i] == ')') {
      --balance;
    } else if (statement[i] == ';') {
      ++semicolons;
    }
    ++i;
  }
  if (balance) {
    printf("Syntax error. Unbalanced parentheses.\n");
    return EXIT_FAILURE;
  } else if (semicolons == 0 && strlen(statement) != 0) {
    printf("Syntax error. Missing semicolon.\n");
    return EXIT_FAILURE;
  } else if (semicolons > 1) {
    printf("Syntax error. Illegal multiple statements.\n");
    return EXIT_FAILURE;
  }

  char *p;
  int keywords_found = 0;
  char kLowerStatementKeywords[3][settings->buffer_statement_size];

  for (int i = 0;
       i < (int)(sizeof kStatementKeywords / sizeof *kStatementKeywords); ++i) {
    int j = 0;
    while (kStatementKeywords[i][j] != '\0') {
      kLowerStatementKeywords[i][j] = (char)tolower(kStatementKeywords[i][j]);
      ++j;
    }
    kLowerStatementKeywords[i][j] = '\0';
    ++j;
  }

  for (int j = 0;
       j < (int)(sizeof kStatementKeywords / sizeof *kStatementKeywords); ++j) {
    if ((p = strstr(statement, kStatementKeywords[j])) != NULL) {
      ++keywords_found;
    } else if ((p = strstr(statement, kLowerStatementKeywords[j])) != NULL) {
      while ((p = strstr(statement, kLowerStatementKeywords[j])) != NULL) {
        int k = 0;
        while (kLowerStatementKeywords[j][k] != '\0') {
          *(p + k) = (char)toupper(*(p + k));
          ++k;
        }
        ++keywords_found;
      }
    }
  }
  if (keywords_found == 0 && strlen(statement) != 0) {
    printf("Syntax error. Unkown statement.\n");
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

typedef struct Column {
  char *title;
  size_t size;
  size_t offset;
  struct Column *next;
} Column;

void AppendColumn(Column **r, char *s, size_t size, size_t offset) {
  Column *new_column = (Column *)malloc(sizeof(Column));
  Column *last_column = *r;
  new_column->title = malloc(sizeof(s));
  strcpy(new_column->title, s);
  new_column->size = size;
  new_column->offset = offset;
  new_column->next = NULL;
  if (*r == NULL) {
    *r = new_column;
    return;
  }
  while (last_column->next != NULL) last_column = last_column->next;
  last_column->next = new_column;
  return;
}

void FreeColumn(Column *r) {
  free(r->title);
  free(r);
}

void FreeColumns(Column **r) {
  Column *current_column = *r;
  Column *next_column;

  while (current_column != NULL) {
    next_column = current_column->next;
    FreeColumn(current_column);
    current_column = next_column;
  }

  *r = NULL;
}
void PrintColumns(Column **r) {
  Column *p = *r;
  while (p != NULL) {
    printf("Columns\t%s|%lu|%lu -> ", p->title, p->size, p->offset);
    p = p->next;
  }
  printf("NULL\n");
}

typedef struct Table {
  char *title;
  int rows;
  Column *column_head;
  size_t row_size;
  int rows_per_page;
  void *page;
  size_t page_size;
  struct Table *next;
} Table;

Table *CreateTable() {
  Table *table = (Table *)malloc(sizeof(Table));
  table->title = NULL;
  table->column_head = NULL;
  table->rows = 0;
  table->row_size = 0UL;
  table->rows_per_page = 0;
  table->page = NULL;
  table->page_size = 32768;
  return table;
}

void AppendTable(Table **table_head, char *s, Table **table_tail) {
  Table *new_table = CreateTable();
  Table *last_table = *table_head;
  new_table->title = malloc(sizeof(s));
  strcpy(new_table->title, s);
  new_table->rows = 0;
  new_table->column_head = NULL;
  new_table->next = NULL;
  if (*table_head == NULL) {
    *table_head = new_table;
    *table_tail = new_table;
    return;
  }
  while (last_table->next != NULL) last_table = last_table->next;
  last_table->next = new_table;
  *table_tail = new_table;
}
void FreeTable(Table *table) {
  FreeColumns(&table->column_head);
  free(table->title);
  free(table->page);
  free(table);
}

void FreeTables(Table **table_head) {
  Table *current_column = *table_head;
  Table *next_column;
  while (current_column != NULL) {
    next_column = current_column->next;
    FreeTable(current_column);
    current_column = next_column;
  }
  *table_head = NULL;
}

void SignalHandler() { exit(EXIT_SUCCESS); }

void SerializeRow(void *dest, void *src, Column **r) {
  Column *p = *r;
  while (p != NULL) {
    memcpy((char *)dest + p->offset, (char *)src + p->offset, p->size);
    p = p->next;
  }
}

void DeserializeRow(void *dest, void *src, Column **r) {
  SerializeRow(dest, src, r);
}

void *RowSlot(Table *table, int row) {
  void *page = table->page;
  if (page == NULL) {
    page = table->page = malloc(table->page_size);
  }
  int byte_offset = row * table->row_size;
  return (char *)page + byte_offset;
}

Table *FindTable(Table **table_head, char *table_title) {
  Table *p = *table_head;
  while (p != NULL) {
    if (strcmp(p->title, table_title) == 0) return p;
    p = p->next;
  }
  return p;
}

void BestPractices(Table *table) {
  for (int i = 0; i < (table->rows); ++i) {
    void *row = malloc(table->row_size);
    DeserializeRow(row, RowSlot(table, i), &table->column_head);
    Column *r = table->column_head;
    while (r != NULL) {
      if (r->size == sizeof(int)) {
        if (r != table->column_head) printf("|");
        printf("%d", *(int *)((char *)row + r->offset));
      } else {
        if (r != table->column_head) printf("|");
        printf("%s", ((char *)row + r->offset));
      }
      if (r->next == NULL) printf("\n");
      r = r->next;
    }

    free(row);
  }
}

void PrintTables(Table **r) {
  Table *p = *r;
  while (p != NULL) {
    printf("Title\t\t%s\n", p->title);
    PrintColumns(&p->column_head);
    BestPractices(p);
    p = p->next;
  }
}

int main() {
  signal(SIGINT, SignalHandler);

  Settings *settings = CreateSettings();

  printf("Enter \".help\" for usage hints.\n");
  printf("Connected to a transient in-memory database.\n");

  Buffer *prompt_buffer = CreateBuffer();

  Table *table_head = NULL;
  Table *table_tail = NULL;

  while (true) {
    PrintPrompt();
    ReadPrompt(prompt_buffer, settings);
    if (settings->verbose)
      printf("Buffer statement\t%s\n", prompt_buffer->statement);
    NormalizeStatement(prompt_buffer->statement, settings);
    if (settings->verbose)
      printf("Normalized prompt_buffer statement\t%s\n",
             prompt_buffer->statement);
    if (strncmp(prompt_buffer->statement, ".help", strlen(".help")) == 0) {
      printf(".verbose\t\tPrint additional details.\n");
    } else if (strncmp(prompt_buffer->statement, ".quit", strlen(".quit")) ==
               0) {
      break;
    } else if (strncmp(prompt_buffer->statement, ".verbose",
                       strlen(".verbose")) == 0) {
      settings->verbose = !settings->verbose;
    } else if (strncmp(prompt_buffer->statement, "CREATE TABLE",
                       strlen("CREATE TABLE")) == 0) {
      char *table_title = malloc(settings->table_title_size * sizeof(char));
      int r = sscanf(prompt_buffer->statement, "CREATE TABLE %s", table_title);
      if (r == 0 || (r == 1 && strncmp(table_title, "(", 1) == 0) ||
          (r == 1 && strncmp(table_title, ";", 1) == 0)) {
        free(table_title);
        printf("Syntax error. Missing table title.\n");
        continue;
      }
      AppendTable(&table_head, table_title, &table_tail);
      if (settings->verbose)
      printf("Table %s was created.\n", table_tail->title);
      free(table_title);

      int columns = 0;
      size_t row_offset = 0;
      int i = 0;
      int j = 0;
      int k = 0;
      bool reading_title = 0;
      bool reading_type = 0;
      char *title = malloc(16 * sizeof(char) + 1);
      char *type = malloc(16 * sizeof(char) + 1);
      int column_size = 0;

      while (prompt_buffer->statement[i] != '\0') {
        if (reading_title) {
          if (prompt_buffer->statement[i] == ' ' && columns == 0) {
            reading_title = 0;
            reading_type = 1;
            title[j] = '\0';
            j = 0;
          } else if (prompt_buffer->statement[i] == ' ' && columns > 0 &&
                     j > 0) {
            reading_title = 0;
            reading_type = 1;
            title[j] = '\0';
            j = 0;
          } else if (prompt_buffer->statement[i] != ' ') {
            title[j] = prompt_buffer->statement[i];
            ++j;
          }
        }

        if (reading_type) {
          if (prompt_buffer->statement[i] == ')' ||
              prompt_buffer->statement[i] == ',') {
            type[k] = '\0';
            if (!strcmp(type, "INT")) {
              column_size = sizeof(int);
              AppendColumn(&table_tail->column_head, title, column_size,
                           row_offset);
              row_offset += column_size;
              table_tail->row_size = row_offset;
            } else if (!strcmp(type, "VARCHAR")) {
              column_size = settings->varchar_size * sizeof(char);
              AppendColumn(&table_tail->column_head, title, column_size,
                           row_offset);
              row_offset += column_size;
              table_tail->row_size = row_offset;
            }

            ++columns;

            reading_title = 1;
            reading_type = 0;
            k = 0;
          } else if (prompt_buffer->statement[i] != ' ') {
            type[k] = prompt_buffer->statement[i];
            ++k;
          }
        }

        if (prompt_buffer->statement[i] == '(') {
          reading_title = 1;
        } else if (prompt_buffer->statement[i] == ')') {
          break;
        }
        ++i;
      }
      free(title);
      free(type);
      if (settings->verbose) printf("Columns created\t\t%d\n", columns);
      if (settings->verbose) PrintColumns(&table_tail->column_head);
      if (columns == 0 || table_tail->row_size == 0) {
        printf("Syntax error. Enter at least one argument.\n");
        continue;
      } else {
        table_tail->rows_per_page =
            table_tail->page_size / (int)table_tail->row_size;
      }
      if (settings->verbose)
        printf("Row size\t\t%lu\nPage size\t\t%lu\nRows per page\t\t%d\n",
               table_tail->row_size, table_tail->page_size,
               table_tail->rows_per_page);
    } else if (strncmp(prompt_buffer->statement, ".tables",
                       strlen(".tables")) == 0) {
      PrintTables(&table_head);
    } else if (strncmp(prompt_buffer->statement, "INSERT INTO",
                       strlen("INSERT INTO")) == 0) {
      char *table_title = malloc(settings->table_title_size * sizeof(char));
      int r = sscanf(prompt_buffer->statement, "INSERT INTO %s", table_title);
      if (r == 0 || (r == 1 && strncmp(table_title, "(", 1) == 0) ||
          (r == 1 && strncmp(table_title, ";", 1) == 0)) {
        free(table_title);
        printf("Syntax error. Missing table title.\n");
        continue;
      }
      Table *table = FindTable(&table_head, table_title);
      if (settings->verbose)
      printf("Table %s found.\n", table->title);
      free(table_title);
      void *row = malloc(table->row_size);
      char *buffer = malloc(settings->varchar_size * sizeof(char));
      int columns = 0;
      Column *ref = table->column_head;
      int int_value = 0;
      int i = 0;

      while (prompt_buffer->statement[i] != '\0') {
        if (prompt_buffer->statement[i] == '(') {
          int j = i;
          int k = 0;
          while (prompt_buffer->statement[j] != ';') {
            if (prompt_buffer->statement[j] == ',' ||
                prompt_buffer->statement[j] == ')') {
              buffer[k] = '\0';
              if ((unsigned long)ref->size == sizeof(int)) {
                int_value = atoi(buffer);
                *((int *)((char *)row + ref->offset)) = int_value;
              } else {
                strcpy(((char *)row + ref->offset), buffer);
              }
              ref = ref->next;
              ++columns;
              k = 0;
            }
            if (prompt_buffer->statement[j] != '(' &&
                prompt_buffer->statement[j] != ' ' &&
                prompt_buffer->statement[j] != ',') {
              buffer[k] = prompt_buffer->statement[j];

              ++k;
            }
            ++j;
          }
          break;
        }
        ++i;
      }

      SerializeRow(RowSlot(table, table->rows), row, &table->column_head);

      table->rows += 1;

      free(row);
      free(buffer);
      free(ref);
    } else if (strncmp(prompt_buffer->statement, "SELECT * FROM",
                       strlen("SELECT * FROM")) == 0) {
      char *table_title = malloc(settings->table_title_size * sizeof(char));
      int r = sscanf(prompt_buffer->statement, "SELECT * FROM %s", table_title);
      if (r == 0 || (r == 1 && strncmp(table_title, "(", 1) == 0) ||
          (r == 1 && strncmp(table_title, ";", 1) == 0)) {
        free(table_title);
        printf("Syntax error. Missing table title.\n");
        continue;
      }
      int i = 0;
      while (table_title[i] != '\0') {
        if (table_title[i] == ';') table_title[i] = '\0';
        ++i;
      }

      Table *table = FindTable(&table_head, table_title);
      if (settings->verbose) printf("Table %s was found.\n", table->title);
      free(table_title);
      BestPractices(table);
    }
  }
  FreeBuffer(prompt_buffer);
  FreeSettings(settings);
  FreeTables(&table_head);
  return EXIT_SUCCESS;
}
