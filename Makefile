CC = clang
CFLAGS = -g -pedantic-errors -W -Wall -Wextra

TARGET = main
O_TARGET = $(TARGET).o

C_FILES = $(wildcard *.c)
O_FILES = $(patsubst %.c, %.o, $(C_FILES))

O_FILES_TARGET = $(O_FILES)

all: $(TARGET) 

$(TARGET): $(O_FILES_TARGET)
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm -f *.o *~ $(TARGET) $(TEST)

format: *.c
	clang-format -style=Google -i *.c
