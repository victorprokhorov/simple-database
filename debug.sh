#!/bin/sh
make clean
make format
make main
valgrind --track-origins=yes \
	--leak-check=full \
	--show-leak-kinds=all \
	./main
